
/**
 * Created by namax on 21/07/16.
 */
class Hello {
    constructor(msg) {
        this.message = msg;
    }

    shoutOut () {
        console.log(this.message);
    }
}

export { Hello };