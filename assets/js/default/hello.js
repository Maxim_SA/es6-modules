import { Hello } from '../lib/hello'
import $ from "jquery"

/**
 * Created by namax on 21/07/16.
 */
class HelloDefault extends Hello {
    constructor(msg) {
        super(msg);
    }

    shoutOut () {
        $("#status").text(this.message+ " From Default");
        console.log(this.message + " From Default");
    }
}

export { HelloDefault };