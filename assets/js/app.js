/**
 * Created by namax on 21/07/16.
 */

import { HelloDefault } from './default/hello'

const hello = new HelloDefault('Hello ES6!');

hello.shoutOut();