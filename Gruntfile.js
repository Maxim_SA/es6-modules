/**
 * Created by namax on 21/07/16.
 */
'use strict';

var webpack = require('webpack');

module.exports = function (grunt) {
    var pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({

        path: {
            bower: "bower_components",
            build: "www/build",
            js: "assets/js",
            css: "assets/css",
            img: "assets/img",
            tmp: "tmp",
        },
        webpack: {
            build: {
                progress: true,
                entry: {
                    app: './<%=path.js%>/app.js'
                },
                output: {
                    path: '<%=path.build%>',
                    filename: 'bundle.js'
                },
                module: {
                    loaders: [{
                        test: /\.js$/,
                        exclude: /node_modules/,
                        loader: 'babel-loader'
                    }]
                },
                resolve: {
                    extenstions: ['', '.js']
                }
            }
        },
        uglify: {
            options: {
                sourceMap: true,
                sourceMapName: './<%=path.build%>/bundle.map'
            },
            build: {
                files: {
                    './<%=path.build%>/bundle.min.js': './<%=path.build%>/bundle.js'
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: "<%=path.img%>",
                    src: ["**/*.{png,jpg,gif}"],
                    dest: "www/images"
                }]
            }
        },
        cssmin: {
            options: {
                keepSpecialComments: 0,
                processImport: true,
                // path to resolve absolute @import rules and rebase relative URLs
                //root: "www",
                sourceMap: false,
                rebase: true
            },
            dist_publicpages: {
                files: {
                    "<%=path.build%>/publicpages.min.css": [
                        "<%=path.build%>/bootstrap3.css",
                        "<%=path.css%>/main.css",
                    ]
                }
            },

        },
        less: {
            options: {
                cleancss: true,
                plugins: [
                    new (require("less-plugin-autoprefix"))({browsers: ["last 2 versions"]})
                ]
            },
            bootstrap3: {
                options: {
                    modifyVars: {
                        "icon-font-path": '"fonts/"'
                    }
                },
                files: {
                    "./<%=path.build%>/bootstrap3.css": "./<%=path.css%>/less/bootstrap3.less"
                }
            }
        },
        watch: {
            js: {
                files: [

                    '/<%=path.js%>/**/*.js',
                ],
                tasks: ['webpack', 'uglify']
            }
        }
    });

    grunt.loadNpmTasks('grunt-webpack');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-contrib-imagemin");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-postcss");
    grunt.loadNpmTasks("grunt-contrib-csslint");
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-cache-bust");

    grunt.registerTask("bootstrap3", ["less:bootstrap3", "cssmin:dist_publicpages"]);
    grunt.registerTask('default', ['webpack', 'uglify', 'cssmin:dist_publicpages', 'cacheBust']);
};